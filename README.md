# publish_partial_trees

***The functionality in publish_partial_trees has been incorporated into ArchivesSpace core code as of v3.0.0-RC2***

An [ArchivesSpace ](http://archivesspace.org/) [plugin](https://github.com/archivesspace/tech-docs/blob/master/customization/plugins.md) to allow appropriately-authorised users to publish or unpublish portions of resource trees. Essentially it copies the functionality of the inbuilt `resources/:id/publish` API route to `archival_objects/:id/publish`, and copies the "Publish All" button from the `frontend/views/shared/_resource_toolbar.html.erb` template to the `frontend/views/shared/_component_toolbar.html.erb` template, then adds corresponding `unpublish` routes for both resources and archival objects. The relevant labels can all be customised in the provided `frontend/locales/en.yml` file. If you make use of other localisations and would like to supply a translation, please do submit a pull/merge request!

This plugin was written for ArchivesSpace v2.7.1. It is likely that it will continue to work for subsequent versions provided that the `archival_objects/:id/publish`, `archival_objects/:id/unpublish` or `resources/:id/unpublish` API routes aren't implemented separately, or the `frontend/views/shared/_component_toolbar.html.erb` and `frontend/views/shared/_resource_toolbar.html.erb` templates don't receive any breaking updates.

## <a name="install">Installation</a>

1. Clone or download the plugin into your ArchivesSpace plugins directory.
2. (Optional) Make changes to the `frontend/locales/en.yml` locale file as required.
3. Add "`publish_partial_trees`" to the `AppConfig[:plugins]` array in your ArchivesSpace config file.
4. Stop and restart ArchivesSpace.
