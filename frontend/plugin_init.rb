ArchivesSpace::Application.extend_aspace_routes(File.join(File.dirname(__FILE__), "routes.rb"))

Rails.application.config.after_initialize do

  class ArchivalObjectsController
  
    set_access_control  "update_resource_record" => [:publish, :unpublish]
  
    def publish
      @archival_object = JSONModel(:archival_object).find(params[:id], find_opts)

      response = JSONModel::HTTP.post_form("#{@archival_object.uri}/publish")

      if response.code == '200'
        flash[:success] = I18n.t("plugins.publish_partial_trees.archival_object._frontend.messages.published", JSONModelI18nWrapper.new(:archival_object => @archival_object).enable_parse_mixed_content!(url_for(:root)))
      else
        flash[:error] = ASUtils.json_parse(response.body)['error'].to_s
      end

      redirect_to "#{request.referer}#tree::archival_object_#{params[:id]}"
    end
    
    def unpublish
      @archival_object = JSONModel(:archival_object).find(params[:id], find_opts)

      response = JSONModel::HTTP.post_form("#{@archival_object.uri}/unpublish")

      if response.code == '200'
        flash[:success] = I18n.t("plugins.publish_partial_trees.archival_object._frontend.messages.unpublished", JSONModelI18nWrapper.new(:archival_object => @archival_object).enable_parse_mixed_content!(url_for(:root)))
      else
        flash[:error] = ASUtils.json_parse(response.body)['error'].to_s
      end

      redirect_to "#{request.referer}#tree::archival_object_#{params[:id]}"
    end
  
  end
  
  class ResourcesController
  
    set_access_control  "update_resource_record" => [:unpublish]
    
    def unpublish
      resource = Resource.find(params[:id])

      response = JSONModel::HTTP.post_form("#{resource.uri}/unpublish")

      if response.code == '200'
        flash[:success] = I18n.t("plugins.publish_partial_trees.resource._frontend.messages.unpublished", JSONModelI18nWrapper.new(:resource => resource).enable_parse_mixed_content!(url_for(:root)))
      else
        flash[:error] = ASUtils.json_parse(response.body)['error'].to_s
      end

      redirect_to request.referer
    end
    
  end
  
end
