ArchivesSpace::Application.routes.draw do

  scope AppConfig[:frontend_proxy_prefix] do
    match 'archival_objects/:id/publish' => 'archival_objects#publish', :via => [:post]
    match 'archival_objects/:id/unpublish' => 'archival_objects#unpublish', :via => [:post]
    
    match 'resources/:id/unpublish' => 'resources#unpublish', :via => [:post]
  end
end

